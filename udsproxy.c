/*
 *
 *
 * compile with "gcc -Wall -o udsproxy udsproxy.c netlib.c util.c"
 */

#include <poll.h>
#include <sys/stat.h>

#define DEBUG

#include "netlib.h"

#define MAX_BUF_SZ 1023
#define IN_PATH "/tmp/bobfcgi.sock"
// #define IN_PATH "/run/bobfcgi/proxy.sock"
#define OUT_PATH "/tmp/bobfcgi2.sock"
// #define OUT_PATH "/run/php/php7.3-fpm.sock"

int read_write (int read_fd, int write_fd, int copy) {
	int nread;
	int ndone = 0;
	char buf[MAX_BUF_SZ + 1];

	nread = read (read_fd, buf, MAX_BUF_SZ);
	while (nread - ndone > 0) {
		// note: this could block under extra-ordinary circumstances, but
		// as we run each connection in a separate process, the impact is
		// minimal
		int nwritten = write (write_fd, buf + ndone, nread - ndone);
		if (nwritten < 0) return nwritten;
		ndone += nwritten;
	}
	if (copy) {
		debug_msg (LOG_DEBUG, "read %d bytes", nread);
		fHexDump (stdout, buf, nread);
	}
	return nread;
} /* read_write */

void full_duplex_copy (int fda, int fdb) {
	struct pollfd fds[2];

	fds[0].fd = fda;
	fds[0].events = POLLIN;
	fds[1].fd = fdb;
	fds[1].events = POLLIN;

	while (1) {
		int ret = poll (fds, 2, 0);
		if (ret > 0) {
			int nba, nbb;

			if (fds[0].revents & POLLIN)
				nba = read_write (fda, fdb, 0);
			if (fds[1].revents & POLLIN)
				nbb = read_write (fdb, fda, 1);
			if ((nba == 0) || (nbb == 0))
				break;
		}
	}
} /* full_duplex_copy () */

int main () {
	int listenfd, in, out;
	char *sockpath = IN_PATH;
    struct stat statbuf;
	mode_t mode;

	listenfd = udsListen (sockpath, 0);
	if (listenfd < 0) {
		debug_msg (LOG_EMERG, "udsListen");
	}
	stat (sockpath, &statbuf);
	mode = statbuf.st_mode;
	mode |= (S_IWGRP | S_IWOTH);
	debug_msg (LOG_DEBUG, "mode: %05o", mode);
	if (chmod (sockpath, mode)) {
		debug_msg (LOG_EMERG, "fchmod");
	}

	in = accept (listenfd, NULL, NULL);
	if (in < 0) {
		debug_msg (LOG_EMERG, "accept");
	}
	
	out = udsConnect (OUT_PATH, 0);
	if (out < 0) {
		debug_msg (LOG_EMERG, "udsConnect");
	}
	full_duplex_copy (in, out);
	close (out);
	close (in);
	close (listenfd);
	return (0);
}
