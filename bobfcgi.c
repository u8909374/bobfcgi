/*
 * bobfcgi.c - Bobs Fast Common Gateway Interface library
 *
 * by Robert (Bob) Edwards, July 2021
 *
 * GPLv2 etc.
 */

#define DEBUG

#include <fastcgi.h>
#include <sys/socket.h>

#include "util.h"
#include "bobfcgi.h"

static int processParams (char *buf, int len, struct bfcgiReq *req) {
	int rv = 0;
	char *tuple = buf;
	int paramLen;

	if (!buf || !req || !req->conn) return -1;

	paramLen = *tuple++;
	if (paramLen & 0x80) {
		paramLen = (paramLen & 0x7f) * 256 + *tuple++;
		paramLen = paramLen * 256 + *tuple++;
		paramLen = paramLen * 256 + *tuple++;
	}
	req->paramBufNext = req->paramBuf;
	while (len > 0) {
		int valueLen = *tuple++;
		if (valueLen & 0x80) {
			valueLen = (valueLen & 0x7f) * 256 + *tuple++;
			valueLen = valueLen * 256 + *tuple++;
			valueLen = valueLen * 256 + *tuple++;
		}
		if (valueLen) {
			char **param = req->conn->paramNames;
			param = req->conn->paramNames;
			while (*param && strncmp (*param, tuple, paramLen)) param++;
			if (*param) {
				char *value = tuple + paramLen;
				if ((req->paramBufNext - req->paramBuf) + valueLen < req->paramBufLen) {
					char *lastval = req->paramBufNext;
					memcpy (lastval, value, valueLen);
					lastval[valueLen] = 0;
					req->paramBufNext += (valueLen + 1);
					req->paramVals[param - req->conn->paramNames] = lastval;
					BDEBUG ("processed %s = %s #%d", *param, lastval, rv);
				} else {
					BWARN ("out of space for %s", *param);
				}
			} else {
				tuple[paramLen] = 0;
				BDEBUG ("param not matched: %s", tuple);
			}
		}
		len -= (2 + paramLen + valueLen);
		tuple += (paramLen + valueLen);
		paramLen = *tuple++;
		rv++;
	}
	return rv;
} /* processParams () */

static int read_x (int fd, char *buf, int len) {
	int rv = 0;
	while (len > 0) {
		int nr = read_timeout (fd, buf, len, 200);
		if (nr <= 0) return -1;
		rv += nr;
		len -= nr;
		buf += nr;
	}
	return rv;
} /* read_x () */

struct bfcgiReq *bfcgiReadReq (struct bfcgiConn *conn) {
	FCGI_Header header;
	char buf[2048];
	struct bfcgiReq *req, *freeReq;
	int reqID;
	int finished = 0;

	if (conn->sock < 0) {
		conn->sock = accept (conn->listSock, NULL, NULL);
		if (conn->sock < 0) {
            BEMERG ("accept");
        }
	}

	while (!finished) {
		int contentLen;
		int nr = read_x (conn->sock, (char *) &header, FCGI_HEADER_LEN);
		if (nr <= 0) {
			BNOTICE ("read");
			return NULL;
		}
		if (header.version != 1) {
			BWARN ("wrong header version: %d", header.version);
			return NULL;
		}
		contentLen = (header.contentLengthB1) * 256 + header.contentLengthB0;
		if (contentLen >= sizeof (buf)) {
			BWARN ("contentLen too large: %d", contentLen);
			return NULL;
		}
		nr = read_x (conn->sock, buf, contentLen + header.paddingLength);
		switch (header.type) {
			case FCGI_BEGIN_REQUEST: /* todo - check this header type... */
				reqID = (header.requestIdB1 * 256) + header.requestIdB0;
				freeReq = NULL;
				for (req = conn->req; req; req = req->next) {
					if (!freeReq && (req->reqID == 0)) freeReq = req;
					BDEBUG ("req: %p, freeReq: %p", req, freeReq);
					if (req->reqID == reqID) break;
				}
				if (!req) {
					if (!freeReq) {
						BWARN ("out of req space");
						return NULL;
					}
					req = freeReq;
					req->reqID = reqID;
					req->paramBufNext = req->paramBuf;
				}
				BDEBUG ("FCGI_BEGIN_REQUEST reqID: %d", reqID);
				break;
			case FCGI_PARAMS: /* Params */
				BDEBUG ("FCGI_PARAMS len: %d", contentLen);
				if (processParams (buf, contentLen, req) < 0) {
					BNOTICE ("processParams returned < 0");
					return NULL;
				}
				break;
			case FCGI_STDIN:
				BDEBUG ("FCGI_STDIN len: %d", contentLen);
				req->inp = buf;
				finished = 1;
				break;
			default:
				BDEBUG ("unrecognised header type: %d, len: %d", header.type, contentLen);
				return NULL;
		}
	}
	return req;
} /* bfcgiReadReq () */

int bfcgiGetQuery (struct bfcgiReq *req, char *query, char *val, int len) {
	if (req->conn->qStrInd < 0) return -1;
	BDEBUG ("qStrInd: %d", req->conn->qStrInd);
	char *p = req->paramVals[req->conn->qStrInd];
	BDEBUG ("query str: %s", p);
	while (*p) {
		char *q = query;
		while (*p && *q && (*p == *q)) {
			p++;
			q++;
		}
		if (!*q && (*p == '=')) {
			p++;
			while (--len && *p && (*p != '&')) {
				*val++ = *p++;
			}
			*val = 0;
			return 0;
		}
		while (*p && (*p != '&')) p++;
		if (*p) p++;
	}
	return -1;
} /* bfcgiGetQuery () */

int bfcgiWriteResp (struct bfcgiReq *req, char *buf, int len) {
	int rv = 0;
	char out[32768];
	FCGI_Header *tHeader = (FCGI_Header *) out;

	tHeader->version = 1;
	tHeader->type = FCGI_STDOUT;
	tHeader->requestIdB1 = req->reqID / 256;
	tHeader->requestIdB0 = req->reqID % 256;
	tHeader->reserved = 0;
	if (!buf) {
		tHeader->type = FCGI_END_REQUEST;
		memset (&out[FCGI_HEADER_LEN], 0, 8);
		buf = &out[FCGI_HEADER_LEN];
		len = 8;
	}
	while (len > 0) {
		int sent = len;
		int padding;
		if (sent > (sizeof (out) - FCGI_HEADER_LEN)) {
			sent = sizeof (out) - FCGI_HEADER_LEN;
		}
		padding = (8 - (sent % 8)) % 8;
		tHeader->contentLengthB1 = sent / 256;
		tHeader->contentLengthB0 = sent % 256;
    	tHeader->paddingLength = padding;
		memcpy (&out[FCGI_HEADER_LEN], buf, sent);
		if (padding) {
			memset (&out[FCGI_HEADER_LEN + sent], 0, padding);
		}
		padding += FCGI_HEADER_LEN + sent;
		debug_hexdump (LOG_DEBUG, "writing:", out, padding);
		write (req->conn->sock, out, padding);
		len -= sent;
		buf += sent;
		rv += sent;
	}
	return rv;
} /* bfcgiWriteResp () */

void bfcgiClose (struct bfcgiConn *conn) {
	if (conn->sock >= 0) {
		close (conn->sock);
		conn->sock = -1;
	}
} /* bfcgiClose () */

struct bfcgiConn *bfcgiNew (int lsock, int nReqs, char **paramNames, int bufLen) {
	if ((nReqs <= 0) || !paramNames) {
		return NULL;
	}

	struct bfcgiConn *new = (struct bfcgiConn *) calloc (1, sizeof (struct bfcgiConn));
	if (!new) {
		return NULL;
	}

	new->listSock = lsock;
	new->sock = -1;
	new->paramNames = paramNames;
	new->qStrInd = strArrInd ("QUERY_STRING", paramNames);

	int nParams = arrCount (paramNames);
	if (nParams < 2) {
		BWARN ("nParams: %d (< 2!)", nParams);
	} else {
		BDEBUG ("nParams: %d", nParams);
	}

	int reqSz = sizeof (struct bfcgiReq) + bufLen + (nParams * sizeof (char *));
	BDEBUG ("reqSz: %d", reqSz);

	void *r = calloc (reqSz, nReqs);
	BDEBUG ("r: %p", r);

	if (r) {
		new->req = (struct bfcgiReq *) r;
		while (nReqs--) {
			struct bfcgiReq *req = (struct bfcgiReq *) r;
			req->conn = new;
			req->paramVals = (char **) r + sizeof (struct bfcgiReq);
			req->paramBuf = (char *) r + sizeof (struct bfcgiReq) + (nParams * sizeof (char *));
			req->paramBufLen = bufLen;
			r += reqSz;
			if (nReqs > 0 ) {
				req->next = (struct bfcgiReq *) r;
			}
		}
		return new;
	}

	free (new);
	return NULL;
} /* bfcgiNew () */

void bcfgiFree (struct bfcgiConn *conn) {
	bfcgiClose (conn);
	if (conn) {
		if (conn->req) free (conn->req);
		conn->req = NULL;
		free (conn);
	}
} /* bcfgiFree () */

void bfcgiDump (struct bfcgiReq *req, FILE *out) {
	struct bfcgiConn *conn = req->conn;
	fprintf (out, "reqID: %d, fd: %d\n", req->reqID, conn->sock);
	for (int i = 0; conn->paramNames[i]; i++) {
		fprintf (out, "  param '%s' = '%s'\n", conn->paramNames[i], req->paramVals[i]);
		if (i == conn->qStrInd) {
			char *qBuf = strdup (req->paramVals[i]);
			if (qBuf) {
				char *qp = qBuf;
				while (qp && *qp) {
					char *saveptr = NULL;
					char *pair = strtok_r (qp, "&", &saveptr);
					char *val = NULL;
					char *name = strtok_r (pair, "=", &val);
					if (*val) {
						fprintf (out, "    '%s' is '%s'\n", name, val);
					} else {
						fprintf (out, "    '%s' has no value\n", name);
					}
					qp = saveptr;
				}
				free (qBuf);
			}
		}
	}
} /* bfcgiDump () */
