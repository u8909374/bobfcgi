/*
 * testfcgi.c - simple test for Bobs FCGI library
 *
 * by Robert (Bob) Edwards, July 2021
 *
 * GPLv2 etc.
 */

#include <sys/socket.h>
#include <sys/stat.h>

#define DEBUG
#define MAX_REQS_CONN 3

#include "netlib.h"
#include "bobfcgi.h"

#define DEFAULT_FCGI_PATH "/run/bobfcgi/bobfcgi.sock"
#define DEFAULT_RESP_HEADER "Content-type: text/html; charset=UTF-8\r\n\r\n" 

char *paramNames[] = {
//    "SCRIPT_FILENAME",
	"QUERY_STRING",
	"REQUEST_METHOD",
//	"CONTENT_TYPE",
//	"CONTENT_LENGTH",
	"SCRIPT_NAME",
//	"REQUEST_URI",
//	"DOCUMENT_URI",
	"DOCUMENT_ROOT",
//	"SERVER_PROTOCOL",
//	"REQUEST_SCHEME",
//	"HTTPS",
//	"GATEWAY_INTERFACE",
//	"SERVER_SOFTWARE",
	"REMOTE_ADDR",
//	"REMOTE_PORT",
//	"SERVER_ADDR",
//	"SERVER_PORT",
	"SERVER_NAME",
//	"REDIRECT_STATUS",
//	"HTTP_HOST",
//	"HTTP_CONNECTION",
//	"HTTP_CACHE_CONTROL",
//	"HTTP_UPGRADE_INSECURE_REQUESTS",
//	"HTTP_USER_AGENT",
//	"HTTP_ACCEPT",
//	"HTTP_ACCEPT_ENCODING",
//	"HTTP_ACCEPT_LANGUAGE",
//	"HTTP_DNT",
//	"HTTP_PRAGMA",
	NULL
};

char *respHeader = DEFAULT_RESP_HEADER; 

int worker (int fd) {
	char buf[2048];
	struct bfcgiConn conn;
	struct bfcgiReq reqs[MAX_REQS_CONN];
	char paramBufs[MAX_REQS_CONN][1024];
	char *paramVals[MAX_REQS_CONN][LEN(paramNames)];

	memset (&conn, 0, sizeof (conn));
	conn.req = reqs;
	for (int i = 0; i < MAX_REQS_CONN; i++) {
		reqs[i].reqID = 0;
		reqs[i].conn = &conn;
		reqs[i].paramVals = paramVals[i];
		reqs[i].paramBuf = paramBufs[i];
		reqs[i].paramBufLen = LEN(paramBufs[0]);
		reqs[i].next = &reqs[i + 1];
	}
	reqs[MAX_REQS_CONN - 1].next = NULL;
	conn.paramNames = paramNames;

	while (1) {
		struct bfcgiReq *req;
		char *p = buf;
		char *getStr;
		if ((conn.sock = accept (fd, NULL, NULL)) < 0) {
			BEMERG ("accept");
		}

		req = bfcgiReadReq (&conn);
		if (!req) {
			close (conn.sock);
			conn.sock = -1;
			continue;
		}
		BDEBUG ("reqID: %d, fd: %d", req->reqID, req->conn->sock);
		for (int i = 0; i < 6; i++) {
			printf ("param %s = %s\n", paramNames[i], req->paramVals[i]);
		}
		bfcgiWriteResp (req, respHeader, strlen (respHeader));
		p += snprintf (p, sizeof (buf) - (p - buf), "<html>\n<head>\n<title>Hi %s</title>\n</head>\n<body>\n", req->paramVals[5]);
		p += snprintf (p, sizeof (buf) - (p - buf), "  <h1>Hi There!</h1>\n  <p>visitor from %s</p>\n", req->paramVals[4]);
		getStr = req->paramVals[0];
		while (getStr && *getStr) {
			char *saveptr = NULL;
			char *pair = strtok_r (getStr, "&", &saveptr);
			char *val = NULL;
			char *name = strtok_r (pair, "=", &val);
			if (*val) {
				p += snprintf (p, sizeof (buf) - (p - buf), "  <p>value of <b>%s</b> is <b>%s</b></p>\n", name, val);
			} else {
				p += snprintf (p, sizeof (buf) - (p - buf), "  <p><b>%s</b> has no value</p>\n", name);
			}
			getStr = saveptr;
		}
		p += snprintf (p, sizeof (buf) - (p - buf), "</body>\n</html>\n");
		bfcgiWriteResp (req, buf, p - buf);
		bfcgiWriteResp (req, NULL, 0);
		close (conn.sock);
		conn.sock = -1;
		req->reqID = 0;
	}
	return 0;
} /* worker () */

int main (int argc, char *argv[]) {
	int fd;
	char *sockpath = DEFAULT_FCGI_PATH;
	struct stat statbuf;
	mode_t mode;

	fd = udsListen (sockpath, 0);
	if (fd < 0) {
		BEMERG ("udsListen");
	}

	if (stat (sockpath, &statbuf)) {
		BEMERG ("stat");
	}
	mode = statbuf.st_mode;
	mode |= (S_IWGRP | S_IWOTH);
	if (chmod (sockpath, mode)) {
		BEMERG ("chmod");
	}

	dbglvl = LOG_NOTICE;
	return worker (fd);
} /* main () */

