CC := gcc
CFLAGS += -Wall
CFLAGS += -D _LOG_LEVEL=7
all: testfcgi udsproxy
version.h: .git/HEAD $(wildcard .git/COMMIT_EDITMSG)
	./mk_version_h.sh > $@
testfcgi: testfcgi.c bobfcgi.c bobfcgi.h netlib.c netlib.h util.c util.h version.h
	$(CC) -o $@ testfcgi.c bobfcgi.c netlib.c util.c
udsproxy: udsproxy.c netlib.c netlib.h util.c util.h
	$(CC) -o $@ udsproxy.c netlib.c util.c

clean:
	rm -f *.o testfcgi udsproxy version.h
