/*
 * bobfcgi.h - Bobs Fast Common Gateway Interface library header
 *
 * by Robert (Bob) Edwards, July 2021
 *
 * GPLv2 etc.
 */

#ifndef __BOBFCGI_H__
#define __BOBFCGI_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef LEN
#define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))
#endif

struct bfcgiConn;

struct bfcgiReq {
	int reqID;
	struct bfcgiConn *conn;
	char **paramVals;
	char *paramBuf;
	int paramBufLen;
	char *paramBufNext;
	char *inp;
	struct bfcgiReq *next;
};

struct bfcgiConn {
	int listSock;
	int sock;
	struct bfcgiReq *req;
	char **paramNames;
	int qStrInd;
};

struct bfcgiReq *bfcgiReadReq (struct bfcgiConn *conn);
int bfcgiGetQuery (struct bfcgiReq *req, char *query, char *val, int len);
int bfcgiWriteResp (struct bfcgiReq *req, char *buf, int len);
void bfcgiClose (struct bfcgiConn *conn);
struct bfcgiConn *bfcgiNew (int lsock, int nReqs, char **paramNames, int bufLen);
void bcfgiFree (struct bfcgiConn *conn);
void bfcgiDump (struct bfcgiReq *req, FILE *out);

#endif
